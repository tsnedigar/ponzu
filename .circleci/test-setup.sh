#!/bin/bash

# Set up test environment

set -ex 

# Install Ponzu CMS
go get -u gitlab.com/tsnedigar/ponzu/...

# test install
ponzu

# create a project and generate code
if [ $CIRCLE_BRANCH = "ponzu-dev" ]; then
        # ensure we have the latest from ponzu-dev branch
        cd /go/src/gitlab.com/tsnedigar/ponzu
        git checkout ponzu-dev
        git pull origin ponzu-dev

        # create new project using the ponzu-dev branch
        ponzu new --dev gitlab.com/tsnedigar/ci/test-project
else 
        ponzu new gitlab.com/tsnedigar/ci/test-project
fi

cd /go/src/gitlab.com/tsnedigar/ci/test-project

ponzu gen content person name:string hashed_secret:string
ponzu gen content message from:@person,hashed_secret to:@person,hashed_secret

# build and run dev http/2 server with TLS
ponzu build

